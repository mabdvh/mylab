import {
  SAVE_TOKEN,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS
} from '../actionTypes';
import Axios from 'axios';
import { BaseUrl } from '../../constants'

export const saveToken = (token) => {
  return { type: SAVE_TOKEN, payload: token }
}

/* 3 State request ke API
  1. Loading ,
  2. Data nya berhasil diperoleh
  3. response error
*/

export const login = (data) => {
  return (dispatch, getState) => {
    dispatch({type: LOGIN_REQUEST})
    Axios({
      method: 'POST',
      url: `${BaseUrl}/login`,
      headers: {
        Accept: 'application/json'
      },
      data: data
    })
    .then(({ data }) => {
      console.warn("response login: ", data)
      // berhasil
      if(data.success) {
        dispatch({type: LOGIN_SUCCESS, payload: data.token })
      } else {
        dispatch({type: LOGIN_ERROR, payload: data.message})
      }
    })
    .catch(err => {
      dispatch({type: LOGIN_ERROR, payload: err.message })
    })
  }
  
}