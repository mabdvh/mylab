import React from "react";
import { View, Text, TextInput, Button, AsyncStorage } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from 'react-redux'

// actions
import {
  saveToken,
  login
} from '../redux/actions'

class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      accessToken: "",
      email: "",
      password: ""
    }
  }

  handleInput = data => {
    let obj = {
      [data.type] : data.text
    }
    this.setState(obj);
  };

  storeData = async (value) => {
    try {
      await AsyncStorage.setItem('token', value)
    } catch (e) {
      // saving error
    }
  }

  async componentDidUpdate(prevProps) {
    if(this.props.auth.token !== prevProps.auth.token) {
      
      this.storeData(this.props.auth.token)
    }
  }

  handleLogin = () => {
    // console.warn("sudah login", this.state.accessToken)
    // this.props.navigation.navigate("Home", {token: this.state.accessToken})
    this.props.login(this.state)
  }

  render() {
    console.warn("token", this.props.auth.token)
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text style={{ marginBottom: 20, fontWeight: "bold" }}>PERSONAL ACCESS TOKEN</Text>
        <TextInput
          onChangeText={text => this.handleInput({type: 'email', text})}
          style={{
            borderWidth: 1,
            borderColor: "#E5E5E5",
            width: 300,
            height: 50,
            borderRadius: 10,
            padding: 10,
            alignItems: "center",
            textAlign: 'center'
          }}
          placeholder="Email"
        />
        <TextInput
          onChangeText={text => this.handleInput({type: 'password', text})}
          style={{
            borderWidth: 1,
            borderColor: "#E5E5E5",
            width: 300,
            height: 50,
            borderRadius: 10,
            padding: 10,
            alignItems: "center",
            textAlign: 'center',
            marginTop: 10
          }}
          placeholder="Password"
          secureTextEntry={true}
        />
        <TouchableOpacity
          onPress={() => this.handleLogin() }
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 20,
              backgroundColor: "#003366",
              width: 300,
              borderRadius: 10,
              height: 50
            }}
          >
            <Text style={{ color: "#87CDEA" }}>LOGIN</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.authReducers
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveToken: (token) => dispatch(saveToken(token)),
    login: (data) => dispatch(login(data))
  }
} 

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
